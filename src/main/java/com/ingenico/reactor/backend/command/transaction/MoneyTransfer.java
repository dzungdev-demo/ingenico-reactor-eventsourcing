package com.ingenico.reactor.backend.command.transaction;

import java.util.List;

import com.ingenico.reactor.backend.common.transactions.CreditRecordedEvent;
import com.ingenico.reactor.backend.common.transactions.DebitRecordedEvent;
import com.ingenico.reactor.backend.common.transactions.FailedDebitRecordedEvent;
import com.ingenico.reactor.backend.common.transactions.MoneyTransferCreatedEvent;
import com.ingenico.reactor.backend.common.transactions.TransferDetails;
import com.ingenico.reactor.backend.common.transactions.TransferState;

import io.eventuate.Event;
import io.eventuate.EventUtil;
import io.eventuate.ReflectiveMutableCommandProcessingAggregate;

public class MoneyTransfer extends ReflectiveMutableCommandProcessingAggregate<MoneyTransfer, MoneyTransferCommand> {

  private TransferDetails details;
  private TransferState state;

  public List<Event> process(final CreateMoneyTransferCommand cmd) {
    return EventUtil.events(new MoneyTransferCreatedEvent(cmd.getDetails()));
  }

  public List<Event> process(final RecordDebitCommand cmd) {
    return EventUtil.events(new DebitRecordedEvent(details));
  }

  public List<Event> process(final RecordDebitFailedCommand cmd) {
    return EventUtil.events(new FailedDebitRecordedEvent(details));
  }

  public List<Event> process(final RecordCreditCommand cmd) {
    return EventUtil.events(new CreditRecordedEvent(details));
  }

  public void apply(final MoneyTransferCreatedEvent event) {
    this.details = event.getDetails();
    this.state = TransferState.INITIAL;
  }

  public void apply(final DebitRecordedEvent event) {
    this.state = TransferState.DEBITED;
  }

  public void apply(final FailedDebitRecordedEvent event) {
    this.state = TransferState.FAILED_DUE_TO_INSUFFICIENT_FUNDS;
  }

  public void apply(final CreditRecordedEvent event) {
    this.state = TransferState.COMPLETED;
  }


  public TransferState getState() {
    return state;
  }
}
