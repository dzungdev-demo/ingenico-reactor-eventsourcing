package com.ingenico.reactor.backend.command.transaction;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import io.eventuate.AggregateRepository;
import io.eventuate.EventuateAggregateStore;
import io.eventuate.javaclient.spring.EnableEventHandlers;
import io.eventuate.local.java.jdbckafkastore.EventuateJdbcEventStoreConfiguration;

@Configuration
@EnableEventHandlers
@Import(EventuateJdbcEventStoreConfiguration.class)
public class MoneyTransferConfiguration {

  @Bean
  public MoneyTransferService moneyTransferService(final AggregateRepository<MoneyTransfer, MoneyTransferCommand> moneyTransferRepository) {
    return new MoneyTransferService(moneyTransferRepository);
  }

  @Bean
  public MoneyTransferWorkflow moneyTransferWorkflow() {
    return new MoneyTransferWorkflow();
  }

  @Bean
  public AggregateRepository<MoneyTransfer, MoneyTransferCommand> moneyTransferRepository(final EventuateAggregateStore eventStore) {
    return new AggregateRepository<MoneyTransfer, MoneyTransferCommand>(MoneyTransfer.class, eventStore);
  }


}
