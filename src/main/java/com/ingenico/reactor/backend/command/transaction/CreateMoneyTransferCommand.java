package com.ingenico.reactor.backend.command.transaction;

import com.ingenico.reactor.backend.common.transactions.TransferDetails;

public class CreateMoneyTransferCommand implements MoneyTransferCommand {
  private final TransferDetails details;

  public TransferDetails getDetails() {
    return details;
  }

  public CreateMoneyTransferCommand(final TransferDetails details) {

    this.details = details;
  }
}
