package com.ingenico.reactor.backend.command.transaction;


import java.util.concurrent.CompletableFuture;

import com.ingenico.reactor.backend.common.accounts.AccountCreditedEvent;
import com.ingenico.reactor.backend.common.accounts.AccountDebitFailedDueToInsufficientFundsEvent;
import com.ingenico.reactor.backend.common.accounts.AccountDebitedEvent;

import io.eventuate.EntityWithIdAndVersion;
import io.eventuate.EventHandlerContext;
import io.eventuate.EventHandlerMethod;
import io.eventuate.EventSubscriber;

@EventSubscriber(id="transferEventHandlers")
public class MoneyTransferWorkflow {

  @EventHandlerMethod
  public CompletableFuture<EntityWithIdAndVersion<MoneyTransfer>> recordDebit(final EventHandlerContext<AccountDebitedEvent> ctx) {
    return ctx.update(MoneyTransfer.class, ctx.getEvent().getTransactionId(), new RecordDebitCommand());
  }

  @EventHandlerMethod
  public CompletableFuture<EntityWithIdAndVersion<MoneyTransfer>> recordDebitFailed(final EventHandlerContext<AccountDebitFailedDueToInsufficientFundsEvent> ctx) {
    return ctx.update(MoneyTransfer.class, ctx.getEvent().getTransactionId(), new RecordDebitFailedCommand());
  }

  @EventHandlerMethod
  public CompletableFuture<EntityWithIdAndVersion<MoneyTransfer>> recordCredit(final EventHandlerContext<AccountCreditedEvent> ctx) {
    return ctx.update(MoneyTransfer.class, ctx.getEvent().getTransactionId(), new RecordCreditCommand());
  }


}
