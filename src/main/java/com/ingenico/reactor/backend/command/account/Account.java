package com.ingenico.reactor.backend.command.account;

import java.math.BigDecimal;
import java.util.List;

import com.ingenico.reactor.backend.common.accounts.AccountCreditedEvent;
import com.ingenico.reactor.backend.common.accounts.AccountDebitFailedDueToInsufficientFundsEvent;
import com.ingenico.reactor.backend.common.accounts.AccountDebitedEvent;
import com.ingenico.reactor.backend.common.accounts.AccountOpenedEvent;

import io.eventuate.Event;
import io.eventuate.EventUtil;
import io.eventuate.ReflectiveMutableCommandProcessingAggregate;

public class Account extends ReflectiveMutableCommandProcessingAggregate<Account, AccountCommand> {

  private BigDecimal balance;

  public List<Event> process(final OpenAccountCommand cmd) {
    return EventUtil.events(new AccountOpenedEvent(cmd.getCustomerId(), cmd.getTitle(), cmd.getInitialBalance(), cmd.getDescription()));
  }

  public List<Event> process(final DebitAccountCommand cmd) {
    if (balance.compareTo(cmd.getAmount()) < 0)
      return EventUtil.events(new AccountDebitFailedDueToInsufficientFundsEvent(cmd.getTransactionId()));
    else
      return EventUtil.events(new AccountDebitedEvent(cmd.getAmount(), cmd.getTransactionId()));
  }

  public List<Event> process(final CreditAccountCommand cmd) {
    return EventUtil.events(new AccountCreditedEvent(cmd.getAmount(), cmd.getTransactionId()));
  }

  public void apply(final AccountOpenedEvent event) {
    balance = event.getInitialBalance();
  }

  public void apply(final AccountDebitedEvent event) {
    balance = balance.subtract(event.getAmount());
  }

  public void apply(final AccountDebitFailedDueToInsufficientFundsEvent event) {
  }

  public void apply(final AccountCreditedEvent event) {
    balance = balance.add(event.getAmount());
  }

  public BigDecimal getBalance() {
    return balance;
  }
}


