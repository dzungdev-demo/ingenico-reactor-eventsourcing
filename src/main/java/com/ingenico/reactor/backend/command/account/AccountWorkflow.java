package com.ingenico.reactor.backend.command.account;


import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;

import com.ingenico.reactor.backend.common.transactions.DebitRecordedEvent;
import com.ingenico.reactor.backend.common.transactions.MoneyTransferCreatedEvent;

import io.eventuate.EntityWithIdAndVersion;
import io.eventuate.EventHandlerContext;
import io.eventuate.EventHandlerMethod;
import io.eventuate.EventSubscriber;

@EventSubscriber(id = "accountEventHandlers")
public class AccountWorkflow {

  @EventHandlerMethod
  public CompletableFuture<?> debitAccount(final EventHandlerContext<MoneyTransferCreatedEvent> ctx) {
    final MoneyTransferCreatedEvent event = ctx.getEvent();
    final BigDecimal amount = event.getDetails().getAmount();
    final String transactionId = ctx.getEntityId();

    final String fromAccountId = event.getDetails().getFromAccountId();

    return ctx.update(Account.class, fromAccountId, new DebitAccountCommand(amount, transactionId)).handle((x, e) -> {
              if (e != null) {
                e.printStackTrace();
              }
              return x;
            }
    );
  }

  @EventHandlerMethod
  public CompletableFuture<EntityWithIdAndVersion<Account>> creditAccount(final EventHandlerContext<DebitRecordedEvent> ctx) {
    final DebitRecordedEvent event = ctx.getEvent();
    final BigDecimal amount = event.getDetails().getAmount();
    final String fromAccountId = event.getDetails().getToAccountId();
    final String transactionId = ctx.getEntityId();

    return ctx.update(Account.class, fromAccountId, new CreditAccountCommand(amount, transactionId)).handle((x, e) -> {
              if (e != null) {
                e.printStackTrace();
              }
              return x;
            }
    );
  }

}
