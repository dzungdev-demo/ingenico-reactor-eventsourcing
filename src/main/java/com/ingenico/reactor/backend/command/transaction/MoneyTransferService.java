package com.ingenico.reactor.backend.command.transaction;

import java.util.concurrent.CompletableFuture;

import com.ingenico.reactor.backend.common.transactions.TransferDetails;

import io.eventuate.AggregateRepository;
import io.eventuate.EntityWithIdAndVersion;

public class MoneyTransferService {
  private final AggregateRepository<MoneyTransfer, MoneyTransferCommand> aggregateRepository;

  public MoneyTransferService(final AggregateRepository<MoneyTransfer, MoneyTransferCommand> aggregateRepository) {
    this.aggregateRepository = aggregateRepository;
  }

  public CompletableFuture<EntityWithIdAndVersion<MoneyTransfer>> transferMoney(final TransferDetails transferDetails) {
    return aggregateRepository.save(new CreateMoneyTransferCommand(transferDetails));
  }

}
