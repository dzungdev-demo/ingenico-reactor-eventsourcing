package com.ingenico.reactor.backend.common.accounts;


import java.math.BigDecimal;

import io.eventuate.Event;

public class AccountOpenedEvent implements Event {

  private String customerId;
  private String title;
  private BigDecimal initialBalance;
  private String description;

  private AccountOpenedEvent() {
  }

  public AccountOpenedEvent(final String customerId, final String title, final BigDecimal initialBalance, final String description) {
    this.customerId = customerId;
    this.title = title;
    this.initialBalance = initialBalance;
    this.description = description;
  }

  public String getCustomerId() {
    return customerId;
  }

  public String getTitle() {
    return title;
  }

  public BigDecimal getInitialBalance() {
    return initialBalance;
  }

  public String getDescription() {
    return description;
  }
}
