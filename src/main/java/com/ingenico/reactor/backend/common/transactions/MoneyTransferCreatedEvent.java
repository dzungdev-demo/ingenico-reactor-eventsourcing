package com.ingenico.reactor.backend.common.transactions;


import io.eventuate.Event;

public class MoneyTransferCreatedEvent implements Event {
  private TransferDetails details;

  private MoneyTransferCreatedEvent() {
  }

  public MoneyTransferCreatedEvent(final TransferDetails details) {
    this.details = details;
  }

  public TransferDetails getDetails() {
    return details;
  }
}

