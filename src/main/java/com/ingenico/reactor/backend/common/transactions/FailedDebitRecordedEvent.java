package com.ingenico.reactor.backend.common.transactions;

import io.eventuate.Event;

public class FailedDebitRecordedEvent implements Event {
  private TransferDetails details;

  private FailedDebitRecordedEvent() {
  }

  public FailedDebitRecordedEvent(final TransferDetails details) {
    this.details = details;
  }

  public TransferDetails getDetails() {
    return details;
  }
}
