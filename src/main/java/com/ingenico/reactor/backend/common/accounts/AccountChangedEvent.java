package com.ingenico.reactor.backend.common.accounts;

import java.math.BigDecimal;

import io.eventuate.Event;

public class AccountChangedEvent implements Event {
  protected BigDecimal amount;
  protected String transactionId;

  public AccountChangedEvent(final BigDecimal amount, final String transactionId) {
    this.amount = amount;
    this.transactionId = transactionId;
  }

  public AccountChangedEvent() {
  }

  public String getTransactionId() {
    return transactionId;
  }

  public BigDecimal getAmount() {
    return amount;
  }
}
