package com.ingenico.reactor.backend.common.accounts;

import java.math.BigDecimal;

public class AccountDebitedEvent extends AccountChangedEvent {

  private AccountDebitedEvent() {
  }

  public AccountDebitedEvent(final BigDecimal amount, final String transactionId) {
    super(amount, transactionId);
  }

}
