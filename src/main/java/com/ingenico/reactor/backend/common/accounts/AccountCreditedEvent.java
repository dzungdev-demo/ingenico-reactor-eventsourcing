package com.ingenico.reactor.backend.common.accounts;

import java.math.BigDecimal;

public class AccountCreditedEvent extends AccountChangedEvent {

  private AccountCreditedEvent() {
  }

  public AccountCreditedEvent(final BigDecimal amount, final String transactionId) {
    super(amount, transactionId);
  }

}
