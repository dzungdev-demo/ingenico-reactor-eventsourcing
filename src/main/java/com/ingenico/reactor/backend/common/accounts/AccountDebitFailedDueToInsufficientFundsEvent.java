package com.ingenico.reactor.backend.common.accounts;

import io.eventuate.Event;

public class AccountDebitFailedDueToInsufficientFundsEvent implements Event {
  private String transactionId;

  private AccountDebitFailedDueToInsufficientFundsEvent() {
  }

  public AccountDebitFailedDueToInsufficientFundsEvent(final String transactionId) {
    this.transactionId = transactionId;
  }

  public String getTransactionId() {
    return transactionId;
  }
}
