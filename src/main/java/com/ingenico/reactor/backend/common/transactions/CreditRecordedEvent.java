package com.ingenico.reactor.backend.common.transactions;

import io.eventuate.Event;

public class CreditRecordedEvent implements Event {
  private TransferDetails details;

  private CreditRecordedEvent() {
  }

  public CreditRecordedEvent(final TransferDetails details) {
    this.details = details;
  }

  public TransferDetails getDetails() {
    return details;
  }
}
