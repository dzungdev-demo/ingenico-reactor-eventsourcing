package com.ingenico.reactor.api.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ingenico.reactor.entity.Transfer;

import reactor.bus.Event;
import reactor.bus.EventBus;

@RestController
@RequestMapping("/api/v1/transfers")
public class TransferApi {
	
	@Autowired
    private EventBus eventBus;
	
	@RequestMapping(method = RequestMethod.POST)
	public String transfer(@RequestBody final Transfer transfer) {
		eventBus.notify("transfer", Event.wrap(transfer));
		return "";
	}
	
}
