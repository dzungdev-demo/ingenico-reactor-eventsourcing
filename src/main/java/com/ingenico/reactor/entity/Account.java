package com.ingenico.reactor.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class Account.
 */
@Entity
public class Account extends AbstractEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 222361901703625920L;

	/** The name. */
	@NotBlank
	@Column(name = "name")
	private String name;

	/** The balance. */
	@NotNull
	@Column(name = "balance")
	private double balance;

	/**
	 * Instantiates a new account.
	 *
	 * @param name
	 *            the name
	 * @param balance
	 *            the balance
	 */
	public Account(final String name, final double balance) {
		super();
		this.name = name;
		this.balance = balance;
	}

	/**
	 * Instantiates a new account.
	 */
	public Account() {
		super();
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Gets the balance.
	 *
	 * @return the balance
	 */
	public double getBalance() {
		return balance;
	}

	/**
	 * Sets the balance.
	 *
	 * @param balance
	 *            the new balance
	 */
	public void setBalance(final double balance) {
		this.balance = balance;
	}

}
