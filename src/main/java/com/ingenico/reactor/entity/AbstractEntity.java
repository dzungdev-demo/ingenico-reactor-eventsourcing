package com.ingenico.reactor.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.springframework.data.jpa.domain.AbstractPersistable;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class AbstractEntity.
 *
 * @param <PK>
 *            the generic type
 */
@MappedSuperclass
public abstract class AbstractEntity<PK extends Serializable> extends AbstractPersistable<PK> {

	/** The version. */
	@Column(name = "version")
	@Version
	@JsonIgnore
	private final Long version = 0L;

}
