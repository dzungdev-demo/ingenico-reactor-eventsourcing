package com.ingenico.reactor.exception;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class NotFoundException.
 */
public class NotFoundException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3511220113866868744L;

	/**
	 * Instantiates a new not found exception.
	 */
	public NotFoundException() {
	}

	/**
	 * Instantiates a new not found exception.
	 *
	 * @param message
	 *            the message
	 */
	public NotFoundException(final String message) {
		super(message);
	}

}
