package com.ingenico.reactor.exception;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class NotPositiveAmountException.
 */
public class NotPositiveAmountException extends Exception {

	/**
	 * Instantiates a new not positive amount exception.
	 */
	public NotPositiveAmountException() {
	}

	/**
	 * Instantiates a new not positive amount exception.
	 *
	 * @param message
	 *            the message
	 */
	public NotPositiveAmountException(final String message) {
		super(message);
	}
}
