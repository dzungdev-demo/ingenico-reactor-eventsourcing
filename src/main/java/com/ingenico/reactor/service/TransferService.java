package com.ingenico.reactor.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ingenico.reactor.entity.Account;
import com.ingenico.reactor.entity.Transfer;
import com.ingenico.reactor.exception.NotPositiveAmountException;
import com.ingenico.reactor.exception.OverDrawnException;
import com.ingenico.reactor.repository.AccountRepository;
import com.ingenico.reactor.repository.IRepository;
import com.ingenico.reactor.repository.TransferRepository;

@Service
@Transactional
public class TransferService extends AbstractService<Transfer> {
	
	/** The transfer repository. */
	@Autowired
	private TransferRepository transferRepository;

	/** The account repository. */
	@Autowired
	private AccountRepository accountRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ingenico.exam.service.AbstractService#getRepository()
	 */
	@Override
	public IRepository<Transfer> getRepository() {
		return transferRepository;
	}

	/**
	 * Save.
	 *
	 * @param transfer
	 *            the transfer
	 * @throws OverDrawnException
	 *             the over drawn exception
	 * @throws NotPositiveAmountException
	 *             the not positive amount exception
	 */
	public void save(final Transfer transfer) throws NotPositiveAmountException, OverDrawnException  {
		if (transfer.getAmount() <= 0) {
			throw new NotPositiveAmountException();
		}
		final Account fromAccount = accountRepository.findById(transfer.getFromAccountId());
		final Account toAccount = accountRepository.findById(transfer.getToAccountId());
		if (fromAccount.getBalance() < transfer.getAmount()) {
			throw new OverDrawnException();
		}
		fromAccount.setBalance(fromAccount.getBalance() - transfer.getAmount());
		toAccount.setBalance(toAccount.getBalance() + transfer.getAmount());
		transfer.setFromAccount(fromAccount);
		transfer.setToAccount(toAccount);

		transferRepository.save(transfer);
	}
	
}
