package com.ingenico.reactor.service;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.AbstractPersistable;

import com.ingenico.reactor.exception.DuplicatedException;
import com.ingenico.reactor.exception.NotFoundException;
import com.ingenico.reactor.repository.IRepository;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class AbstractService.
 *
 * @param <T>
 *            the generic type
 */
public abstract class AbstractService<T extends AbstractPersistable<Long>> {

	/** The message source. */
	@Autowired
	private MessageSource messageSource;

	/**
	 * Gets the repository.
	 *
	 * @return the repository
	 */
	public abstract IRepository<T> getRepository();

	/**
	 * Find all.
	 *
	 * @return the list
	 */
	public List<T> findAll() {
		return getRepository().findAll();
	}

	/**
	 * Find all.
	 *
	 * @param pageable
	 *            the pageable
	 * @return the page
	 */
	public Page<T> findAll(final Pageable pageable) {
		return getRepository().findAll(pageable);
	}

	/**
	 * Find one.
	 *
	 * @param id
	 *            the id
	 * @return the t
	 */
	public T findOne(final Long id) {
		return getRepository().findOne(id);
	}

	/**
	 * Delete by primary key.
	 *
	 * @param id
	 *            the id
	 * @throws NotFoundException
	 *             the not found exception
	 */
	public void delete(final Long id) throws NotFoundException {
		final T element = getRepository().findOne(id);
		if (element == null) {
			throw new NotFoundException();
		}
		getRepository().delete(id);

	}

	/**
	 * Save list of entity.
	 *
	 * @param elements
	 *            the elements
	 * @return List<T> the list of entity
	 * @throws DuplicatedException
	 *             the duplicated exception
	 */
	public List<T> save(final List<T> elements) throws DuplicatedException {
		return getRepository().save(elements);
	}

	/**
	 * Count.
	 *
	 * @return the long
	 */
	public long count() {
		return getRepository().count();
	}

	/**
	 * Msg.
	 *
	 * @param msgCode
	 *            the msg code
	 * @param objs
	 *            the objs
	 * @return the string
	 */
	public String msg(final String msgCode, final Object... objs) {
		return messageSource.getMessage(msgCode, objs, Locale.US);
	}

}
