package com.ingenico.reactor.repository;

import com.ingenico.reactor.entity.Transfer;

/**
 * @author DUNG TAN DANG
 * 
 *         The Interface TransferRepository.
 */
public interface TransferRepository extends IRepository<Transfer> {

}
