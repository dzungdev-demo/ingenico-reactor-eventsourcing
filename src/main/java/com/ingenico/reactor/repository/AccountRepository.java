package com.ingenico.reactor.repository;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.query.Param;

import com.ingenico.reactor.entity.Account;

/**
 * @author DUNG TAN DANG
 * 
 *         The Interface AccountRepository.
 */
public interface AccountRepository extends IRepository<Account> {

	/**
	 * Find by name.
	 *
	 * @param name
	 *            the name
	 * @return the account
	 */
	Account findByName(String name);

	/**
	 * Find by id.
	 *
	 * @param id
	 *            the id
	 * @return the account
	 */
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	Account findById(@Param("id") Long id);

}
