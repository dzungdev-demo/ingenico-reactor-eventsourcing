package com.ingenico.reactor.repository;

import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author DUNG TAN DANG
 * 
 *         The Interface IRepository.
 *
 * @param <T>
 *            the generic type
 */
public interface IRepository<T extends AbstractPersistable<Long>>
		extends JpaRepository<T, Long>, JpaSpecificationExecutor<T> {

}
