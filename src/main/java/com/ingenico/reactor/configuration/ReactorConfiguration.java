package com.ingenico.reactor.configuration;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.ingenico.reactor.entity.Transfer;
import com.ingenico.reactor.service.TransferService;

import reactor.bus.Event;
import reactor.bus.EventBus;
import reactor.bus.selector.Selectors;
import reactor.fn.Consumer;
import reactor.spring.context.config.EnableReactor;

@Configuration
@EnableReactor
public class ReactorConfiguration {

	@Autowired
	private EventBus eventBus;
	
	@Autowired
	private TransferService transferService;

	@PostConstruct
	public void onStartUp() {
		eventBus.on(Selectors.R("transfer"), transfer());
	}
	
	private Consumer<Event<Transfer>> transfer() {
        return logInfoEvent -> {
        	try {
        		System.err.println("==========> run in consumer");
				transferService.save(logInfoEvent.getData());
			} catch (final Exception e) {
				e.printStackTrace();
			}
        };
    }

}
