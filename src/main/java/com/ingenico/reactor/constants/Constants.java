package com.ingenico.reactor.constants;

/**
 * @author DUNG TAN DANG
 * 
 *         The Interface Constants.
 */
public interface Constants {

	/** The namespace uri. */
	String NAMESPACE_URI = "http://ingenico.com/exam/api/soap/schemas";

}
