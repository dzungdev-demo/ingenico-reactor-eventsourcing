package com.ingenico.reactor.constant;

/**
 * @author DUNG TAN DANG
 * 
 *         The Interface Constants.
 */
public interface Constants {

	/** The dzung account id. */
	Long DZUNG_ACCOUNT_ID = 1L;

	/** The dzung account name. */
	String DZUNG_ACCOUNT_NAME = "Dzung";

	/** The hong account id. */
	Long HONG_ACCOUNT_ID = 2L;

	/** The hong account name. */
	String HONG_ACCOUNT_NAME = "Hong";

	/** The transfers api base path. */
	String TRANSFERS_API_BASE_PATH = "/api/v1/transfers";

	/** The accounts api base path. */
	String ACCOUNTS_API_BASE_PATH = "/api/v1/accounts";
}