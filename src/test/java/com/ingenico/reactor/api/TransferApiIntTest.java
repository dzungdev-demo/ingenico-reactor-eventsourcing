package com.ingenico.reactor.api;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.ingenico.reactor.App;
import com.ingenico.reactor.api.rest.TransferApi;
import com.ingenico.reactor.constant.Constants;
import com.ingenico.reactor.entity.Transfer;
import com.ingenico.reactor.service.TransferService;
import com.ingenico.reactor.util.TestUtil;

import reactor.bus.EventBus;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class TransferApiIntTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
@WebAppConfiguration
@IntegrationTest({ "server.port=0", "management.port=0" })
@Transactional
@TestPropertySource(locations = { "classpath:test-application.properties" })
public class TransferApiIntTest {

	/** The transfer service. */
	@Autowired
	private TransferService transferService;
	
	@Autowired
    private EventBus eventBus;

	/** The rest transfer api mock mvc. */
	private MockMvc restTransferApiMockMvc;

	/**
	 * Setup.
	 */
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		final TransferApi transferApi = new TransferApi();
		ReflectionTestUtils.setField(transferApi, "eventBus", eventBus);

		restTransferApiMockMvc = MockMvcBuilders.standaloneSetup(transferApi).build();
	}

	/**
	 * Test given dzung account balance 1 mil when 500 transfer requests each
	 * time 50 then successfully.
	 *
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testGivenDzungAccountBalance1Mil_when500TransferRequestsEachTime50_thenSuccessfully()
			throws IOException, Exception {

		final Transfer transfer = new Transfer(Constants.DZUNG_ACCOUNT_ID, Constants.HONG_ACCOUNT_ID, 50);

		restTransferApiMockMvc.perform(post(Constants.TRANSFERS_API_BASE_PATH)
							  .content(TestUtil.convertObjectToJsonBytes(transfer))
							  .contentType(MediaType.APPLICATION_JSON_UTF8))
							  .andExpect(status().isCreated());

	}

//	/**
//	 * Test given hong account balance 100 when transfer from hong 150 then http
//	 * status ERROR is return.
//	 *
//	 * @throws IOException
//	 *             Signals that an I/O exception has occurred.
//	 * @throws Exception
//	 *             the exception
//	 */
//	@Test
//	public void testGivenHongAccountBalance100_whenTransferFromHong150_thenHttpStatusERRORIsReturn()
//			throws IOException, Exception {
//		final Transfer transfer = new Transfer(Constants.HONG_ACCOUNT_ID, Constants.DZUNG_ACCOUNT_ID, 150);
//
//		restTransferApiMockMvc.perform(post(Constants.TRANSFERS_API_BASE_PATH)
//							  .content(TestUtil.convertObjectToJsonBytes(transfer))
//							  .contentType(MediaType.APPLICATION_JSON_UTF8))
//							  .andExpect(status().isInternalServerError());
//	}

}
