package com.ingenico.reactor.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ingenico.reactor.AbstractIntegrationTest;
import com.ingenico.reactor.entity.Transfer;
import com.ingenico.reactor.exception.NotPositiveAmountException;
import com.ingenico.reactor.exception.OverDrawnException;
import com.ingenico.reactor.repository.AccountRepository;

/**
 * @author DUNG TAN DANG
 * 
 *         The Class TransferServiceIntTest.
 */
public class TransferServiceIntTest extends AbstractIntegrationTest{

	/** The transfer service. */
	@Autowired
	private TransferService transferService;

	/** The account repository. */
	@Autowired
	private AccountRepository accountRepository;

	/** The thread pool size. */
	private final int threadPoolSize = 30;

	/** The run times. */
	private final int runTimes = 502;

	
	
//	@Test
//	public void testTransferFromAtoBAndBToA_thenNoDeadlock() {
//		
//		
//		final ExecutorService executorService = Executors.newFixedThreadPool(70);
//		final List<Future<Void>> futures = new ArrayList<Future<Void>>();
//		for (int x = 0; x < 1000; x++) {
//			final Callable<Void> callable = new Callable<Void>() {
//				@Override
//				public Void call() throws Exception {
//					final Transfer dzungToHongTransfer = new Transfer(1L, 2L, 100);
//					final Transfer hongToDzungTransfer = new Transfer(2L, 1L, 1);
//					TransferServiceIntTest.this.transferService.save(dzungToHongTransfer);
//					TransferServiceIntTest.this.transferService.save(hongToDzungTransfer);
//					return null;
//				}
//			};
//			final Future<Void> submit = executorService.submit(callable);
//			futures.add(submit);
//		}
//		final List<Exception> exceptions = new ArrayList<Exception>();
//		for (final Future<Void> future : futures) {
//			try {
//				future.get();
//			} catch (final Exception e) {
//				exceptions.add(e);
//				e.printStackTrace();
//			}
//		}
//		
//	}
	
	@Test
	public void test() throws NotPositiveAmountException, OverDrawnException {
		final Transfer dzungToHongTransfer = new Transfer(1L, 2L, 100);
		transferService.save(dzungToHongTransfer);
	}

}
